﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScript : MonoBehaviour {
    public int life;

    public GameObject target;
    private NavMeshAgent agent;

    public float pathfindTolarance;

    public void Damage(int damage)
    {
        life -= damage;

        if (life <= 0)
        {
            FindObjectOfType<PlayerScript>().score++;
            Destroy(gameObject);
        }
    }

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        if (target)
        {
            agent.SetDestination(target.transform.position);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (target)
        {
            float d = (target.transform.position - agent.destination).magnitude;
            if (d > pathfindTolarance)
            {
                agent.SetDestination(target.transform.position);
            }
        }
		
	}
}
