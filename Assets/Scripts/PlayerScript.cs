﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    private Animator animator;
    public CharacterController controller;
    public float speed;
    public float turnSpeed;

    public GameObject CannonBall;
    public Transform FiringPosition;

    public int score;

	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
    
	
	// Update is called once per frame
	void Update () {

        float forward = Input.GetAxis("Vertical");

        animator.SetFloat("speed", forward);

        float turn = Input.GetAxis("Horizontal");

        controller.SimpleMove(forward * speed * transform.forward);

        transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);

        if (Input.GetButtonDown("Fire1"))
        {
            animator.SetTrigger("fire");
            Instantiate(CannonBall, FiringPosition.position, FiringPosition.rotation);
        }
		
	}
}
