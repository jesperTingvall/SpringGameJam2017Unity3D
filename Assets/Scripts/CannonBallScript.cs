﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallScript : MonoBehaviour {

    private Rigidbody rigidbody;
    public float speed;
    public float lifetime;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.velocity = speed * transform.forward;
        Destroy(gameObject, lifetime);
	}

    private void OnCollisionEnter(Collision collision)
    {
        EnemyScript enemy = collision.gameObject.GetComponent<EnemyScript>();

        if (enemy != null)
        {
            enemy.Damage(1);
        }
        Destroy(gameObject);


    }

    // Update is called once per frame
    void Update () {
		
	}
}
